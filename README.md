# 在线考试系统

联系作者：18207700421


#### 介绍
一个在线考试系统，
考生可以注册，成为本平台的一个用户，然后进行考试，
考完生成成绩，同时用户可以查询自己考试的试卷，可以查看试卷解析。

-- 升级改版。
 新增出卷人角色，主要职责是进入系统，出试卷，设置题目，发布考试通知。
 阅卷人角色，主要职责是进入系统改卷，由系统（使用随机算法）随机分配一个阅卷人进行改卷。
 当考生开始考生，新增考试时间，在规定的时间内完成考试，否则自动退出。

#### 登录界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/002033_67c3bbda_5362924.png "login.png")


#### 主页面
![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/002104_6cf2be33_5362924.png "main.png")

#### 答题试卷
![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/002200_a55ab7c8_5362924.png "card.png")

#### 试卷解析
![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/002226_a748a795_5362924.png "paper.png")

#### 软件架构
软件架构说明


#### 安装教程


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
