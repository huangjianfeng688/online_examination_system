package com.baidu.exam.controller;

import com.baidu.exam.except.MyException;
import com.baidu.exam.model.ExamUser;
import com.baidu.exam.service.IExamUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: samsung
 * Date: 2019/9/14
 * Time: 14:03
 * Description:
 *
 * @author admin
 */
@Controller
@RequestMapping("/user")
public class ExamUserController {
    @Autowired
    private IExamUserService iExamUserService;

    private static Logger logger = LoggerFactory.getLogger(ExamUserController.class);

    //去登录界面,
    @RequestMapping("/tologin")
    public String tologin() {
        logger.info("登录用户名");
        return "user/login";
    }

    //实现登录功能.
    @RequestMapping("/login")
    public String login(String uname, String upwd) {
        //查询该用户是否存在.
        logger.info("登录用户名：{}", uname);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(uname, upwd);
        try {
            subject.login(token);
        } catch (AuthenticationException e) {
            throw new AuthenticationException("用户名或者密码错误");
        }
        return "redirect:/home/papers.action";
    }

    //去注册界面
    @RequestMapping("/toregister")
    public String toregister() {

        return "user/register";
    }

    //注册功能
    @RequestMapping("/register")
    public String register(String uname, String upwd) {
        iExamUserService.saveExamUser(new ExamUser(uname, upwd));
        return "redirect:tologin.action";
    }
}
