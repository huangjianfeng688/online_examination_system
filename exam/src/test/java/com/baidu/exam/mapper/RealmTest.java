package com.baidu.exam.mapper;

import com.baidu.exam.model.ExamUser;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.crypto.hash.AbstractHash;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Test;

/**
 * <p>
 *
 * </p>
 *
 * @package: com.baidu.exam.mapper
 * @description:
 * @className: RealmTes
 * @author: @nncskj admin
 * @date: Created in 2022/5/31
 * @copyright: Copyright (c) 2022/5/31
 * @version: V1.0
 */
public class RealmTest {
    @Test
    public void test01() {
        SimpleHash simpleHash = new SimpleHash("md5", "123456");
        simpleHash.setIterations(3);
        String s = simpleHash.toString();
        System.out.println("s = " + s);
        HashedCredentialsMatcher md5 = new HashedCredentialsMatcher("MD5");
        SimpleHash hash = new SimpleHash("md5", "123456","",3);
        System.out.println("hash.toString() = " + hash.toString());


        DefaultPasswordService defaultPasswordService = new DefaultPasswordService();

        DefaultHashService hashService = new DefaultHashService();
        hashService.setHashAlgorithmName("md5");
        hashService.setHashIterations(3);
        defaultPasswordService.setHashService(hashService);

        Hash s1 = defaultPasswordService.hashPassword("123456");
        System.out.println("s1 = " + s1.toString());

    }
}
